## Amazon Web Services (AWS)

### Historique, philosophie et R&D

#### Historique: la (vraie) légende

![](swiftonsec.png)

Comme souvent dans l'univers de l'_IT_, _Amazon Web Services_ c'est une fantastique réussite qui n'était pas prévue. Les _Web Services_ d'Amazon étaient initialement une sorte  de preuve de concept, une panoplie, une collection d'outils que les e-commerçants qu'étaient alors Amazon avaient imaginé de livrer à leur partenaires.

Au regard de l'incroyable effort d'industrialisation que déploie Amazon aujourd'hui pour automatiser et optimiser chaque maillon de sa chaine de production, on peu difficilement imaginer qu'au début des années 2000, l'ancêtre d'"AWS" n'est en réalité qu'un bric à brac de boites à outils diverses et variées qui vise les marchands tels que Target ou Marks & Spencer aux USA via une plateforme dédiée, `merchant.com`.
À travers cet embryon de "software as service", la société s'aperçoit qu'elle doit muter vers un modèle différent, et alors que le monde entier découvre l'Internet, des employés d'Amazon sont en train de réfléchir à un modèle de livraison logicielle et matérielle plus souple, plus rapide, plus agile...
Il faut se remettre en situation, à cette époque, ce qui deviendra le monstre _AWS_ n'est qu'une portion infime de la société Amazon dont la tâche n.1 est de vendre sur Internet, mais pourtant, entre 2000 et 2003, c'est en pionniers que Benjamin Black et Chris Pinkham imaginent les premières briques d'une infrastructure totalement standardisée, automatisée et reposant intensivement sur des Web Services pour des applications telles que le stockage. Une révolution.

L'idée est présentée à Jeff Bezos, fondateur et patron de Amazon; pendant cette mythique réunion, les compétences clé de la société sont mises sur la table, en particulier la capacité des hommes clés à mettre en oeuvre des architectures durables, sûres, optimisées et au meilleur prix. Bezos trouve bonne l'idée de créer un centre de service de haute disponibilité articulé autour de services Web, ou plus précisément des API, et il accepte que les expérimentations aient lieu avec le matériel même d'Amazon. Les discussions sont passionnées et ambitieuses, Andy Jassy, CEO de la structure AWS entrevoit le futur et parle de finalement créer "le système d'exploitation de l'Internet"

Et lorsqu'on connait l'histoire d'Internet lui même, on ne peut que s'émerveiller de l'analogie entre les deux réussites mondiales. En effet, le succès du réseau global tel que nous le connaissons est le fruit de la collaboration de plusieurs génies qui lors d'un fabuleux coup de poker, prophétisent sur ce que sera l'Internet 10 ans plus tard.

C'est en 2004, sans fanfare, que le blog officiel "Amazon Web Services" est lancé. Le service n'est alors qu'une collection d'API permettant d'accéder à plusieurs services d'Amazon, mais on s'extasie déjà de pouvoir interagir avec la société de façon programmatique, en utilisant 

![Le premier blog post](1st_AWS_Blog_Post.png)

![Le premier web service](Jeff_Barr_web_service_model_1.png)

Puis à la surprise générale, en Novembre 2004, le premier service global d'Amazon Web Services: SQS (Simple Queue Service), encore bien en vie aujourd'hui, est exposé à une poignée de partenaires. Des mots de Jeff Barr, évangéliste et employé de la première heure, la réaction générale fut "Huh ? Pourquoi Amazon ferait ça ?". Le monde de l'IT est décoré de petites phrases issues des analystes les plus férus prédisant la déchéance de technologies qui pourtant plus tard seront louées par ces mêmes analystes.

En 2005, la société commence à donner accès aux technologies sur lesquelles elle travaille secrètement en contrepartie de _non-disclosure agreements_, et parallèlement à cette confrontation au monde réel, prépare son annonce publique. Ces technologies ne sont ni plus ni moins que S3, le système de stockage d'objets à la durabilité inégalée, mais aussi et surtout le joyau EC2 qui a en réalité vu le jour en Afrique du Sud, sous l'expertise de Chris Pinkham qui avait imposé à la société ce choix de vie qui, selon son propre avoeu, aura été en réalité le catalyseur du succès de Elastic Compute Cloud, ou EC2. Et c'est ainsi que contre toute attente, techniquement au moins, l'Afrique du sud fût la première "région" AWS ;-)

C'est finalement en 2006 que commence réellement l'Histoire, à la diffusion en production de _S3_, _SQS_, et finalement _EC2_ en version beta.

Anecdote à retenir, en 2010, l'intégralité du site Amazon.com a finalement migré sur _AWS_, et la boucle fût bouclée.

#### La (fausse) légende

Peut-être le lecteur aura-t-il entendu dans des conférences ou lu dans des magazines d'"experts" une toute autre version de l'histoire : la fausse légende, qui agace énormément les créateurs d'AWS, raconte qu'Amazon ne sachant plus quoi faire de son surplus de machines lié à l'important _turnover_ technologique, a embrassé le mouvement de la virtualisation et commencé à recycler son ancien stock en imaginant un modèle de location de puissance CPU l'heure. On comprend que la réalité est beaucoup granulaire et progressive, les techniciens parmi les lecteurs savent bien que cela est naturel pour un projet de cette envergure.

#### Stratégie

Quelques chiffres pour comprendre intuitivement la stratégie implacable d'AWS.

En 2016 :

* $12 Milliards de chiffre d'affaire
* $3 milliards de profit
* 90% des sociétés du Fortune 100 utilisent AWS
* 75% de marges sur les MSP
* Marché de $77 Milliards lié aux _professional services_

Avec toute les précautions nécessaires à la lecture des prédictions des analystes, Gartner Group annonce qu'un Trilliard de dollars sera dépensé dans le mouvement migratoire vers le Cloud dans les 5 ans à venir(1), et le message pilonné par AWS lors de tous ses _summits_ est très clair: **l'heure est à la migration !** et Amazon compte bien s'appuyer sur ses dizaines de milliers de partenaires pour parvenir à mettre la main sur cette moitié d'Internet qui lui échappe encore.

![Cloud Market Share 2017](cloud-market-share-2017.png)

L'argumentaire est rôdé et alléchant, des milliards de dollars à se partager entre tous les MSP de la place, il y en aura pour tout le monde ! Car c'est cela aussi, la puissance d'Amazon, plus besoin d'être un nom reconnu du secteur pour rafler la mise, c'est même plutôt l'inverse, les grosses structures n'ont plus le vent en poupe, trop protectionnistes, trop sclérosées, pas assez... agiles ! Et elle est la, la stratégie, dans l'agilité, Amazon a très tôt compris l'intérêt de l'intégration continue, et dans sa recherche des marges les plus fines, la souplesse et la rapidité des nouveaux modèles de chaine d'intégration a pris de court les mastodontes pour laisser émerger de nouveaux acteurs, plus jeunes, baignés dans cette mouvance, habitués au partage et aux innovations incessantes.

Sur le front connexe de ces méthodologies, Amazon a rapidement compris le potentiel "geekesque" de son Cloud, les cibles des betas tests des premières versions de Web Services étaient d'ailleurs des développeurs, aussi, pour asseoir sa position de leader mais aussi pour faire passer ses concurrents pour des "has been", la société a rapidement misé sur un système de formations aussi redoutable que convoité; en jouant du réseau social, Amazon met en avant ses certifiés et crée l'envie du coté des novices, la passion et l'implication du côté des diplomés.

![AWS Certification path](AWS_Certification_path.png)

Et pour s'assurer que chaque développeur a le loisir de mettre en pratique ses connaissances théoriques, mais aussi pour permettre à chacun de tester réellement AWS, la société met à disposition de l'utilisateur un programme "Free Tier", qui permet l'utilisation de certains composants gratuitement pendant un an. Ainsi, sans dépenser un seul centime, l'explorateur pourra s'entrainer à créer une infrastructure de catégorie professionnelle pendant une année entière.

Cette proximité aux développeurs et aux techniciens, AWS la cultive, et ils le lui rendent bien. On ne compte plus le nombre de comptes GitHub peuplés de contributions, voir de solutions complètes et Libres, plus épatant, des entreprises milliardaires telles que NetFlix, second client AWS après Amazon.com, ont montré la voix en publiant des outils stratégiques tels que le Chaos Monkey, légendaire logiciel qui détruit volontairement mais au hasard, des équipements virtuels de l'infrastructure NetFlix afin de s'assurer qu'elle est parfaitement redondante. Qui aurait misé sur une symbiose aussi forte il y a une dizaine d'années ?

Poussant cette logique à l'extrême, AWS a une fois encore montré la voie en créant le mouvement serverless en 2014 à l'aide de briques innovantes telles que Lambda et API Gateway, et comme d'habitude, ce furent d'abord les développeurs s'approprièrent le produit, donnant parfois lieu à des projets d'envergure colossales tels que le framework _serverless.com_.
Cette technologie est un acte important dans la pièce informatique, car si le pari est gagné, et de très beaux noms de l'industrie se sont déjà engouffré dans le mouvement, c'est effectivement tout un éco-système, celui des hébergeurs et infogérants, qui se trouve au bord de l'extinction et de la désuétude. Le _serverless_ est la quintessence du mouvement DevOps et de l'intégration continue, c'est la disparition pure et simple de la notion de "serveur" au sens historique du terme. Évidemment, le code continuera de s'exécuter sur des machines, fussent-elles virtuelles, containers, ou composants, mais le pari, c'est que ces éléments infrastructurels soient le cadet de nos soucis, l'infrastructure est un moyen comme un autre et son existence devient éphémère, le temps de l'exécution d'une fonction qui durera quelques milisecondes... c'est presque poétique.

#### R&D: l'ouverture comme stratégie

Comme on le comprend à la lumière du nombre d'innovations disruptives issues des laboratoires d'AWS, le budget alloué à la Recherche et au Développement est simplement faramineux : Amazon est **la** société IT qui a investit le plus dans sa R&D au monde en 2016 avec un total cumulé à travers les années de 14 Milliards de dollars. Difficile de lutter.

![Dépenses en R&D des géants de l'IT](AWS_RD.png)

Et ça paye ! depuis son trône de leader, Amazon donne le ton, invente, publie, communique, il est d'ailleurs amusant de constater que, jouant de son status et de sa puissance de communication, Amazon a pratiquement imposé un vocabulaire technologique qu'il a pour partie inventé, et il est toujours surprenant de retrouver chez ses concurrents une terminologie finalement similaire et parfois identique.

Par l'intermédiaire de son charismatique CTO Wernet Wogels, chaque année à la grand messe AWS de Las Vegas, re:Invent, ce sont des dizaines de produits toujours plus innovants qui sont annoncés en fanfare et sous les applaudissements des plus de 30000 spectateurs, curieux, partenaires, certifiés et fans. Et à chaque annonce, c'est un coup de massue supplémentaire sur ses concurrents, condamnés pour le moment au statut de suiveurs, mais parfois et c'est souvent surprenant, sur ses partenaires. En effet, il n'est pas rare que Amazon annonce en grande pompe un produit qui va venir compléter la panoplie des outils déjà très avancés de la gestion de sa plateforme, et que ce produit soit en concurrence frontale avec une solution jusqu'alors proposée par un partenaire. Prenons pour exemple un produit annoncé à re:Invent 2016, X-Ray, dont l'objectif est d'aider les développeurs (évidemment) à détecter les anomalies provoquant des ralentissements ou des dysfonctionnements dans leurs applications hébergées sur AWS. Ce produit entre frontalement en concurrence avec des solutions telles que NewRelic ou encore Zend Server, à cette différence près que X-Ray est évidemment parfaitement intégré à l'éco-système AWS et qu'il sera evidemment bien plus simple à mettre en oeuvre que des solutions tierce partie.

L'engouement des foules, mais en particulier des techniciens, pour ces solutions n'est pas uniquement le fait de l'effet d'annonce, mais également d'une remarquable entreprise d'ouverture. Nous l'avons vu au début de cette analyse, AWS, c'est d'abord la mise à disposition au public d'un API, et finalement cette approche n'a pas changé avec le temps, tout est API chez Amazon, tout s'interroge à distance au travers de multiples langages de programmations grace à des SDK (Software Development Kit) d'excellente qualité, toujours munis d'une documentation impeccable et d'exemples concrets. De la création de machines virtuelles (instances dans le vocabulaire AWS) au redimensionnement de base de données en passant par le dialogue avec Alexa, le service de voix d'Amazon, tout se programme, tout s'interroge, tout se _monitore_. Avec de tels outils en main, il est naturel de voir pulluler sur les divers sites de partage de code tels que GitHub des contributions à n'en plus finir. Et Amazon donne l'exemple ! Dans le dépôt GitHub "awslabs" on trouve des centaines de produits directement issus du travail des ingénieurs Amazon, des exemples, des produits complets, des tutoriels, des preuves de concept, le bonheur du développeur, la caverne d'Ali Baba du DevOps. Et bien évidemment, pour les produits plus officiels tels que les SDK ou les composants intégrant la plateforme AWS, le compte officiel expose le code source auquel tout un chacun est en mesure de participer.

Amazon sait parler au geeks, et c'est probablement ce qui fait la différence avec les autres fournisseurs de cloud public, pour dernier exemple, et parce ce domaine est devenu un business model à part entière, l'entreprise n'a pas abordé l'IoT de façon _corporate_ en inondant les magazines de buzzwords, au lieu de cela, Amazon a rendu l'expérience ludique et a immédiatement permis aux techniciens de s'amuser avec cette nouvelle génération d'outils. Dans un univers qui fantasme de mêler réel et virtuel, ces jouets pour developpeurs s'interfacent programmatiquement avec les différentes briques du Cloud AWS. Il en est ainsi pour "AWS IoT button", une simple "télécommande" munie d'un seul bouton qui permet de contrôler _Lambda_, _DynamoDB_ ou encore _SNS_ (nous parlerons en détail de ces technologies plus tard dans cette analyse), mais aussi pour _Echo_, ce cylindre qui répond au son de votre voix.

![AWS IoT button](aws_iot_button.png)

![Echo & Echo Dot](echo_feature._CB526147196_.png)

### La plateforme

#### Régions

Nous entrons dans le vif du sujet et utiliserons au fil de cette présentation le vocabulaire propre à Amazon. De la plus grosse unité au composant le plus infime, AWS, c'est d'abord de la terminologie, et une proportion non négligeable des certifications Associate s'assurent que le candidat "parle AWS" !

L'infrastructure AWS, ce sont, à l'heure ou j'écris ces lignes, 16 **Régions** en production et 3 en préparation. Ce terme qui parait banal représente la première et plus grosse unité de la plateforme AWS.

![AWS Global Infrastructure](Global_Infrastructure_kwV22.png)

Une Région est un emplacement géographique sur le globe dans lequel Amazon possède au moins deux **Availability Zones**, ou dans le langage du commun des mortels: un datacenter.

![AWS AZs](AWS_AZs.png)

Non seulement du fait du nombre d'Availability Zones mais également en raison du nombre de fonctionnalités, toutes les régions ne sont pas égales. En effet, certaines régions peuvent disposer d'une solution avant les autres, et certaines pourraient attendre plusieurs mois, voire années avant de les proposer au public. Ce fut le cas pour _EFS_, un service NFS, d'abord disponible en Oregon et seulement disponible 1 an plus tard pour quelques autres régions.

Les services AWS sont hébergés dans des Availability Zones qui sont propriété l'Amazon, ce sont des datacenters banalisés dont très peu de photos, soumises au controle d'Amazon, circulent sur Internet. Ces AZs (Availability Zones) sont bien evidemment reliées entre elles via des connections acceptant des débits très élevés. Bien qu'il soit relativement difficile d'obtenir des informations sur la signification du mot "élevé", une présentation interne de la société annonçait des liens inter-région (entre AZ, donc) montant à 25Tbps avec des latences de 1ms, pour une capacité totale de 102Tb (4x la capacité d'une liaison inter-AZ) pour chaque datacenter. En 2014. Ces chiffres donneraient le tournis à n'importe quel administrateur réseau.

![AWS datacenter de Virginie](aws-cloud-fog-web-1.jpg)

![Bande passante inter-rgion](aws-hamilton-two.jpg)

#### Edge locations

En addition aux AZs, l'infrastructure AWS s'étend sur des points de présence (PoP) qu'ils appellent **Edge Locations**. Ces _Edge Locations_, au contraire des _Availability Zones_, ne sont pas forcement dans des datacenters appartenant à Amazon. Ces _PoP_ accueillent des services nécessitant une présence plus dense comme le _CDN_ CloudFront, le _DNS_ Route 53 ou encore le _WAF_ AWS.

![Edge Locations](edge_locations.png)

### Services clé

#### EC2

Actuel joyau de la couronne, _Elastic Compute Cloud_ est le service le plus connu. Souvent injustement réduit à un service de machines virtuelles (instances), _EC2_ est en réalité un tout cohérent. Comme on peut souvent le lire, à travers les capacités d'_EC2_, ce sont des datacenters virtuels entiers qui sont à portée de main, avec des fonctionnalités aussi nombreuses que puissantes.

L'unité la plus importante d'_EC2_ c'est le _VPC_, ou _Virtual Private Cloud_. C'est une segmentation logique, une _sorte_ de _VLAN_ dans lequel on crée ses ressources. C'est dans le _VPC_ que résident les sous réseaux, les instances, les bases de données...
Un _VPC_ est propre à une _région_, comprendre qu'un _VPC_ s'étale sur toutes les _Availability Zones_ de la région. C'est l'objet de plus haut niveau de votre architecture, celui auquel on attachera les passerelles vers Internet ou vers vos réseaux _on premise_.

Dans ce _VPC_, on crée évidemment des sous réseaux non routés sur internet, qui eux seront associés à une _Availability Zone_. Et dans ces sous réseaux, nous créons les instances _EC2_, terminologie _Amazonienne_ pour nommer une machine virtuelle.
Ces instances existent en de nombreuses variétés, notons en particulier :

* Les instances M3 et M4, pour une utilisation générique, munies de processeurs puissants _E5-26xx_, juste équilibre entre mémoire, puissance de calcul et capacité de stockage.

* R3 et R4, optimisées pour l'utilisation intensive de la mémoire vive (cache, base de données...)
* I3, disposant de disques _SSD_ soutenus par de la _NVMe_ (Non Volatile Memory express) et destinés aux applications utilisant intensivement le système de fichiers (traitements lourds sur de très nombreux fichiers...)
* C3 et C4, les instances disposant de processeurs de dernière génération au prix le plus abordable, destinés aux applications très demandeuses en temps CPU (langages interprétés, calculs, ...)
* T2, les instances les plus économiques, dont la fameuse _t2.micro_, éligible au programme _Free Tier_. Ce sont des instances qui sont typiquement destinées au test ou aux applications peu intensives car leur modèle de facturation repose sur des "crédits" en temps processeur qu'il est possible de gagner lorsque ce dernier n'est pas utilisé.

Mais bien au delà des instances, _EC2_ c'est surtout le nirvana du _DevOps_, la panacée de l'intégration continue, la promesse réalisée de l'infrastructure élastique : l'_Auto Scaling_.  
L'_Auto Scaling_, c'est la capacité d'une architecture à croître de façon autonome, automatique, fonction d'un critère défini dans _CloudWatch_, le système de métriques et d'alarmes de _AWS_. Ainsi, on peut ordonner à un _Auto Scaling Group_ d'ajouter un ou plusieurs nouveaux serveurs dans l'architecture pour ajouter plus de capacité pendant un temps donné, par exemple fonction de la charge processeur.  
Imaginons un site _e-commerce_ une matinée de soldes, en ayant placé un seuil d'alerte à 60% d'utilisation du processeur, l'_ASG_ (_Auto Scaling Group_) démarrera de nouvelles instances lorsque la charge aura atteint ce seuil, ajoutant autant de capacité, et les détruira lorsque le seuil bas sera atteint à son tour.  

C'est grâce à ces techniques qu'_AWS_ se permet d'annoncer des gains financiers de belle envergure à ses clients, car en effet, lorsque ces derniers n'utilisaient en réalité que 40% des ressources de leurs serveurs dédiés chez un hébergeur classique, ils en payaient la totalité, là ou dans un scénario de plateforme élastique, uniquement les ressources nécessaires seront utilisées et facturées.

#### RDS

Révolu le temps de l'installation, maintenance et recherche de la valeur à mettre dans la 28ème variable du fichier de configuration de _MySQL_. _RDS_ (pour _Relational Database Service_) est un service de base de données relationnelles géré par AWS.  
Il se décline en plusieurs moteurs :

* MySQL
* MariaDB, un _fork_ de MySQL réalisé par son auteur originel
* Aurora, une version AWS de MySQL qui annonce des performances en moyenne multipliées par 5
* PostgreSQL
* Microsoft SQL Server
* Oracle

Avec _RDS_, la création d'une base de données _Master / Slave_ avec bascule automatique en cas de dysfonctionnement, application automatique des correctifs mineurs et _backups_ automatisés s'effectue en 5 minutes via la console d'administration, la ligne de commande _AWS CLI_ ou encore l'_API_. Par d'administration, pas d'accès _shell_ aux serveurs, c'est un service entièrement géré par _AWS_, avec les bons et les mauvais côtés, car vous n'aurez pas la souplesse d'aller _fine tuner_ ce petit paramètre du système d'exploitation dont la légende dit qu'il accélère les accès en écriture de la base de 0.8%. Mais est-ce bien grave ?

#### S3

#### Route 53

#### CloudFront

#### DynamoDB

#### Lambda

### Test de la console AWS

### Conclusion

Sources:

https://techcrunch.com/2016/07/02/andy-jassys-brief-history-of-the-genesis-of-aws/
https://en.wikipedia.org/wiki/Timeline_of_Amazon_Web_Services
http://www.zdnet.com/article/how-amazon-exposed-its-guts-the-history-of-awss-ec2/
https://aws.amazon.com/blogs/aws/aws-blog-the-first-five-years/
http://blog.b3k.us/2009/01/25/ec2-origins.html
https://www.slideshare.net/AmazonWebServices/keynote-awspartnersummitsingapore2017
https://www.enterprisetech.com/2014/11/14/rare-peek-massive-scale-aws/

Ref:

(1) http://www.gartner.com/newsroom/id/3384720
